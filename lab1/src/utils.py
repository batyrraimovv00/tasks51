def getAnswer():
    return 42

class Counter:
    def __init__(self, cnt_value = 0):
        self.cnt = cnt_value
        
        
    def inc(self):
        self.cnt += 1
        
        
    def dec(self):
        self.cnt -= 1
        
        
    def get_value(self):
        return self.cnt