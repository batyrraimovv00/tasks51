import sys
sys.path.insert(0,"src")
import math
import pytest

from square import Squareti

def test_square_zero():
    S = Squareti()
    assert S.square(0) == math.sqrt(0) 

def test_square_one():
    S = Squareti()
    assert S.square(1) == math.sqrt(1)
    
def test_square_minus():
    S = Squareti()
    with pytest.raises(ValueError):
        S.square(-25)
        
def test_square_big():
    S = Squareti()
    assert S.square(999999999999999999999999999999999999678) == math.sqrt(999999999999999999999999999999999999678)  
    
def test_square_super():
    S = Squareti()
    assert S.square(0.00001) == math.sqrt(0.00001)
